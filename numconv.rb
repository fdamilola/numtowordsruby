require 'sinatra'
require 'sinatra/reloader'
require 'json'
require './test'

get '/' do
	return "<b>Number to words is a rest service for converting numbers to their word equivalent. use making GET or POST requests of the format <u> http://BASEURL+/NumberService?num=number</u></b>"
end

get '/NumberService/?*' do
	h = Hash.new()
	content_type('application/json')
	value = params['num']
	k = conv(value)
	h["number"] = value
	h["status"] = k[1]
	h["result"] = k[0]
	return h.to_json()
end

post '/NumberService/?*' do
	h = Hash.new()
	content_type('application/json')
	value = params['num']
	k = conv(value)
	h["number"] = value
	h["status"] = k[1]
	h["result"] = k[0]
	return h.to_json()
end


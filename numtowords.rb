	class NumToWord
		def initialize(number)
			@number = number.to_i
		end

		def convToWord()
			wordrep = ""
			splitSize = 3
			chunks = splitToChunks(@number.to_s().reverse(), splitSize)
			chunksize = chunks.length()
			#puts chunks.to_json()
			for i in (chunksize-1).downto(0) do
				#puts i
				threedigno = chunks[i].to_i()
				hun = threedigno/100
				if hun > 0
					#puts "hun", hun
					wordrep+=getWordRep(hun).to_s
					wordrep+=" Hundred"
				end

				ten = threedigno%100

				if (ten > 0 && i == 0)
					#puts "and based on ten"
					if (hun > 0 || chunksize > 1)
						#puts "and based on ten added"
						wordrep+=" and "
					end
				elsif (ten > 0 && i > 0 && hun > 0)
					wordrep+=" and "
				end

				if (ten > 10 && ten <= 19)
					wordrep+=(" "+getWordRep(ten).to_s)
				else
					ten=ten/10
					if (ten > 0)
						wordrep+=(getWordRep(ten*10).to_s)
					end

					unit = ((threedigno%100)%10).to_i
					#puts unit	
					if (unit > 0 && ten > 0)
						wordrep+="-"
					end

					if (unit > 0)
						wordrep+=getWordRep(unit).to_s
					end
				end
	
				if threedigno > 000
					wordrep+=getPlaceHolders(i).to_s
				end

			end
			# puts threedigno 
			if (wordrep == "")
				return [wordrep+".", "failed"]
			else
				return [wordrep+".", "success"]
			end
		end
	end

	def rev(string)
		return string.reverse()
	end

	def splitToChunks(string, chunksize)
		origlen = string.length()
		chunks = Array.new()
		i = 0
		while i < origlen do
			c = rev(string[i, chunksize])
			chunks << c
			#puts [i, c, [origlen, (i + chunksize)].min()].to_json
			i+=3
		end
		#puts chunks.size()
		return chunks 
	end


	def getWordRep(number)
		reps = {1=> "One", 2=> "Two", 3=> "Three", 4=> "Four", 5=> "Five", 6=> "Six",
				7=> "Seven", 8=> "Eight", 9=> "Nine", 10=> "Ten", 11=> "Eleven", 12=> "Twelve",
				13=> "Thirteen", 14=> "Fourteen", 15=> "Fifteen", 16=> "Sixteen", 17=> "Seventeen", 
				18=> "Eighteen", 19=> "Nineteen", 20=> "Twenty", 30=> "Thirty", 40=> "Forty",
				50=> "Fifty", 60=> "Sixty", 70=> "Seventy", 80=> "Eighty", 90=> "Ninty"}
			return reps[number]
	end

	def getPlaceHolders(number)
		placeHolders = {1=> " Thousand ", 2=> " Million ", 3=> " Billion ", 
								4=> " Trillion ", 5=> " Quadrillion ", 6=> " Quintillion "}
		return placeHolders[number]
	end